<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     block_adaptajulho
 * @category    string
 * @copyright   2022 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['adaptajulho:addinstance'] = 'Adicionar instância do bloco Adapta Julho';
$string['adaptajulho:myaddinstance'] = 'Adicionar instância do bloco Adapta Julho no Painel';
$string['addedsuccess'] = 'Registro salvo com sucesso.';
$string['adding'] = 'Adding';
$string['addingtitle'] = 'Adding title';
$string['addnew'] = 'Add new';
$string['anothertext'] = 'Another text in another page';
$string['configlabel'] = 'Minha configuração';
$string['configdescription'] = 'Minha descrição de configuração';
$string['foo'] = 'Foo';
$string['errorreserved'] = 'Nome indisponível. Por favor, escolha outro.';
$string['example'] = 'Example';
$string['id'] = 'ID';
$string['indextitle'] = 'My index title';
$string['pluginname'] = 'Bloco Adapta Julho';
$string['textlabel'] = 'Configuração de texto';
$string['textdescription'] = 'Descrição de texto';
$string['updatedsuccess'] = 'Updated successfully.';
