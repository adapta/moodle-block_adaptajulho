<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * List of all records of block_adaptajulho.
 *
 * @package     block_adaptajulho
 * @copyright   2022 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url('/blocks/adaptajulho/index.php'));
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('indextitle', 'block_adaptajulho') . $SITE->fullname);
$PAGE->set_heading(get_string('pluginname', 'block_adaptajulho'));

$PAGE->navbar->add(get_string('preview'), new moodle_url('/a/link/if/you/want/one.php'));

$output = $PAGE->get_renderer('block_adaptajulho');
$renderable = new \block_adaptajulho\output\index($USER->id);

echo $output->header(),
     $output->render($renderable),
     $output->footer();
