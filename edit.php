<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * List of all records of block_adaptajulho.
 *
 * @package     block_adaptajulho
 * @copyright   2022 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');

require_login();

$id = optional_param('id', 0, PARAM_INT);

$context = context_system::instance();
$PAGE->set_context($context);
$url = new moodle_url('/blocks/adaptajulho/edit.php', ['id' => $id]);
$PAGE->set_url($url);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('addingtitle', 'block_adaptajulho') . $SITE->fullname);
$PAGE->set_heading(get_string('pluginname', 'block_adaptajulho') . ' - ' . get_string('adding', 'block_adaptajulho'));

$form = new \block_adaptajulho\output\form\edit();

if ($form->is_cancelled()) {
    redirect(new moodle_url('/blocks/adaptajulho/index.php'));
} else if ($fromform = $form->get_data()) {

    $record = (object)[
	'name' => $fromform->name,
	'userid' => $USER->id,
	'usermodified' => $USER->id,
	'timemodified' => time(),
	'timecreated' => time(),
	'deleted' => 0,
    ];
    if ($id) {
	$record->id = $id;
	$DB->update_record('block_adaptajulho', $record);
	    redirect(new moodle_url('/blocks/adaptajulho/index.php'),
		     get_string('updatedsuccess', 'block_adaptajulho'));
    } else {
	if ($id = $DB->insert_record('block_adaptajulho', $record)) {
	    redirect(new moodle_url('/blocks/adaptajulho/index.php'),
		     get_string('addedsuccess', 'block_adaptajulho'));
	}
    }
}
if ($id) {
    $record = $DB->get_record('block_adaptajulho', ['id' => $id]);
    $form->set_data($record);
}
echo $OUTPUT->header(),
     $form->render(),
     $OUTPUT->footer();
