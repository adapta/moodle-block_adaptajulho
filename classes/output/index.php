<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Index page renderable for block_adaptajulho
 *
 * @package     block_adaptajulho
 * @copyright   2022 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_adaptajulho\output;
use renderable;
use renderer_base;
use templatable;
use stdClass;
use moodle_url;

class index implements renderable, templatable {
    /** @var string $sometext Algum texto que será passado para o template. */
    var $sometext = null;

    public function __construct($userid) {
        $this->userid = $userid;
    }

    /**
     * Exporta os dados para serem usados como um contexto dos templates mustache.
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
	global $DB;
	$sql = 'SELECT aj.*, u.firstname, u.lastname, u.email
		  FROM {block_adaptajulho} aj
                  JOIN {user} u
                    ON u.id = aj.userid
		 WHERE (userid = ? OR userid = ?)
		   AND aj.deleted <> ?';

	$params = [$this->userid, $this->userid, 1];
	$records = $DB->get_records_sql($sql, $params);

        $data = new stdClass();
	$data->records = [];
	foreach ($records as $r) {
	    $r->editurl = new moodle_url('/blocks/adaptajulho/edit.php', ['id' => $r->id]);
	    $data->records[] = $r;
	}
	$data->addurl = new moodle_url('/blocks/adaptajulho/edit.php');
        return $data;
    }
}
