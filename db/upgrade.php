<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin upgrade steps are defined here.
 *
 * @package     block_adaptajulho
 * @category    upgrade
 * @copyright   2022 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute block_adaptajulho upgrade from the given old version.
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_block_adaptajulho_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    // For further information please read {@link https://docs.moodle.org/dev/Upgrade_API}.
    //
    // You will also have to create the db/install.xml file by using the XMLDB Editor.
    // Documentation for the XMLDB Editor can be found at {@link https://docs.moodle.org/dev/XMLDB_editor}.
    if ($oldversion < 2022071200) {

        // Define table block_adaptajulho to be created.
        $table = new xmldb_table('block_adaptajulho');

        // Adding fields to table block_adaptajulho.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('usermodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table block_adaptajulho.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $table->add_key('userid', XMLDB_KEY_FOREIGN, ['userid'], 'user', ['id']);
        $table->add_key('usermodified', XMLDB_KEY_FOREIGN, ['usermodified'], 'user', ['id']);

        // Adding indexes to table block_adaptajulho.
        $table->add_index('name', XMLDB_INDEX_NOTUNIQUE, ['name']);
        $table->add_index('userid_name', XMLDB_INDEX_NOTUNIQUE, ['userid', 'name']);

        // Conditionally launch create table for block_adaptajulho.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Adaptajulho savepoint reached.
        upgrade_block_savepoint(true, 2022071200, 'adaptajulho');
    }

    if ($oldversion < 2022071201) {

        // Define field deleted to be added to block_adaptajulho.
        $table = new xmldb_table('block_adaptajulho');
        $field = new xmldb_field('deleted', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0', 'timemodified');

        // Conditionally launch add field deleted.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

	// Define index deleted (not unique) to be added to block_adaptajulho.
	$index = new xmldb_index('deleted', XMLDB_INDEX_NOTUNIQUE, ['deleted']);

        // Conditionally launch add index deleted.
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        // Adaptajulho savepoint reached.
        upgrade_block_savepoint(true, 2022071201, 'adaptajulho');
    }

    if ($oldversion < 2022071202) {

        // Define table block_adaptajulho_coisas to be created.
        $table = new xmldb_table('block_adaptajulho_coisas');

        // Adding fields to table block_adaptajulho_coisas.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
	$table->add_field('description', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);

        // Adding keys to table block_adaptajulho_coisas.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for block_adaptajulho_coisas.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Adaptajulho savepoint reached.
        upgrade_block_savepoint(true, 2022071202, 'adaptajulho');
    }

    return true;
}
